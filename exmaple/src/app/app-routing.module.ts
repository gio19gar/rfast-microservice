import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginNoticiasModule } from './login-noticias/login-noticias.module';
import { ParametersModule } from './parameters/parameters.module';

const routes: Routes = [{
  path:'login',
  loadChildren: ()=>import('./login-noticias/login-noticias.module').then(m=>LoginNoticiasModule)
},{
  path:'parametros',
  loadChildren: ()=>import('./parameters/parameters.module').then(m=>ParametersModule)
},{
  path:'**',
  redirectTo:'login'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
