import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { LoginNoticiasRoutingModule } from './login-noticias-routing.module';
import { ScreensComponent } from './screens/screens.component';
import { FormComponent } from './components/form/form.component';
import { NewsComponent } from './components/news/news.component';




@NgModule({
  declarations: [
    ScreensComponent,
    FormComponent,
    NewsComponent
  ],
  imports: [
    CommonModule,
    LoginNoticiasRoutingModule,
    ReactiveFormsModule,
    RouterModule
  ]
})
export class LoginNoticiasModule { }
