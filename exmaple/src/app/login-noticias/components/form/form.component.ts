import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Conexion } from 'src/app/parameters/interfaces/interfaces';
import { ConexionesService } from '../../../services/conexiones.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  private _conexiones:Conexion[] = [];

  constructor(private fb:FormBuilder, private router:Router, private conexionService:ConexionesService) { }

  ngOnInit(): void {
    this.conexionService.getAllConexiones().subscribe(resp => {
      this._conexiones = [...resp];
    })
  }

  public myForm:FormGroup = this.fb.group({
    'id_client' : ['147',[Validators.required,Validators.minLength(3)]],
    'email' : ['soporte@r-fast.com',[Validators.required,Validators.email]],
    'password': ['900073714R-FAST147',[Validators.required,Validators.minLength(3)]],
    'connection': ['',[Validators.required]]
  });


  public login():void {
    
    const {connection}= this.myForm.value;

    localStorage.setItem('baseDeDatos',connection);

    this.router.navigateByUrl('/parametros');
  }

  get conexiones():Conexion[]{
    return [...this._conexiones];
  }
}
