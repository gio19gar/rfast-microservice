import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ScreensComponent } from './screens/screens.component';

const routes: Routes = [{
  path:'',
  children: [{
    path: 'loginNews',
    component: ScreensComponent
  },{
    path: '**',
    redirectTo:'loginNews'
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginNoticiasRoutingModule { }
