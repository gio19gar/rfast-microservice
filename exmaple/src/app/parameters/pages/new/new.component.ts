import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Params, Router } from '@angular/router';
import { ConPCaja } from '../../interfaces/interfaces';
import { MatDialog } from '@angular/material/dialog';
import { QuestionComponent } from '../../../message/question/question.component';
import { ConpcajaService } from '../../services/conpcaja.service';
import { Observable, switchMap, tap } from 'rxjs';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {
  public readonly:boolean = false;
  public modificar:boolean = false;
  private conPCaja:ConPCaja= {
    codigo_caja:   '',
    codigo_filial: '',
    nombre_caja: '',
    codigo_puc:  ''
  }

  constructor(
      private fb:FormBuilder, 
      private router:Router, 
      private dialog:MatDialog,
      private cajaService:ConpcajaService,
      private activatedRoute:ActivatedRoute
  ) { 
  }

  ngOnInit(): void {

    if(!this.router.url.includes('edit') ){
      return;
    }

    this.conPCaja = {
      codigo_caja:   '',
      codigo_filial: '',
      nombre_caja: '',
      codigo_puc:  ''
    }

    this.activatedRoute.params.pipe(
      tap(console.log),
      switchMap<Params,Observable<ConPCaja>>( ({codigo_caja,codigo_filial}) => this.cajaService.getConPCajasPk(codigo_caja,codigo_filial)),
      tap(console.log)
    ).subscribe( resp =>{
      this.myForm.setValue({...resp});
      this.readonly = true;
    });



    this.modificar = true;
    
    this.myForm.setValue ({
      ...this.conPCaja
    })
    
  }

  public myForm:FormGroup = this.fb.group({
    'codigo_caja':[this.conPCaja.codigo_caja,[Validators.required,Validators.maxLength(4)]],
    'codigo_filial':[this.conPCaja.codigo_filial,[Validators.required,Validators.maxLength(2)]],
    'nombre_caja':[this.conPCaja.nombre_caja,[Validators.required,Validators.maxLength(40)]],
    'codigo_puc':[this.conPCaja.codigo_puc,[Validators.required,Validators.maxLength(32)]]
  });

  save(){
    const dialog = this.dialog.open(QuestionComponent,{
      data: {
        accion: !this.router.url.includes('edit')? 'Crear':'Modificar'
      }
    });
    dialog.afterClosed().subscribe(
      result=>{
        if(result){
          if(!this.router.url.includes('edit')){
            this.saveForm();
          }else{
            this.editForm();
          }
          console.log("Guardar esta informacion");
        }else{
          console.log("No guardar");
        }
      }
    )
  }

  public saveForm():void{
    this.cajaService.saveConPCaja({...this.myForm.value}).subscribe((resp)=>{
      this.router.navigateByUrl('/parametros');
    });
  }

  public editForm():void{
    this.cajaService.updateConPCaja({...this.myForm.value}).subscribe((resp)=>{
      this.router.navigateByUrl('/parametros');
    });
  }

  validarError(campo:string):boolean{
    return !!this.myForm.controls[campo].errors && this.myForm.controls[campo].touched;
  }

  get errorCodigoCaja():string{
    return this.textError('codigo_caja');
  }

  get errorCodigoFilial():string{
    return this.textError('codigo_filial');
  }

  get errorNombreCaja():string{
    return this.textError('nombre_caja');
  }

  get errorCodigoPuc():string{
    return this.textError('codigo_puc');
  }

  public textError(campo:string):string{
    let msg:string = '';
    if(this.myForm.controls[campo].errors?.['required']) msg = 'Campo es requerido';
    if(this.myForm.controls[campo].errors?.['maxlength']) msg = `Campo debe ser menor a ${this.myForm.controls[campo].errors?.['maxlength'].requiredLength}`;
    return msg;
  }
}
