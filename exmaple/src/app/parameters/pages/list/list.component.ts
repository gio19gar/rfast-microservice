import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog'
import { QuestionComponent } from 'src/app/message/question/question.component';
import { ConpcajaService } from '../../services/conpcaja.service';
import { ConPCaja } from '../../interfaces/interfaces';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent {

  private _cajas:ConPCaja[] = [];

  constructor(public dialog: MatDialog, private conpcajaService:ConpcajaService){}
  
  public delete(conPcaja:ConPCaja):void {
    const dialog = this.dialog.open(QuestionComponent,{
      data: {
        accion: 'borrar'
      }
    });

    dialog.afterClosed().subscribe(
      result=>{
        if(result){
          this.conpcajaService.deleteById(conPcaja).subscribe((resp)=>{this.getCajas()});
        }
      }
    )
  }

  ngOnInit(): void {
    this.getCajas();
  }

  get cajas():ConPCaja[]{
    return [...this._cajas];
  }

  public getCajas(){
    this.conpcajaService.getAllConPCaja().subscribe((resp)=> this._cajas = resp);
  }

  
}
