import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ConPCaja } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class ConpcajaService {

  private url:string = environment.urlservice;
  private paramtDb = '?tenantId=';

  constructor(private http:HttpClient) { }

  getAllConPCaja():Observable<ConPCaja[]> {
    const db = localStorage.getItem('baseDeDatos');
    const url:string = `${this.url}${this.paramtDb}${db}`;
    return this.http.get<ConPCaja[]>(url);
  }

  public saveConPCaja(conpcaja:ConPCaja):Observable<ConPCaja>{
    const db = localStorage.getItem('baseDeDatos');
    const url:string = `${this.url}${this.paramtDb}${db}`;
    return this.http.post<ConPCaja>(url,{...conpcaja});
  }

  public deleteById(conpcaja:ConPCaja):Observable<any>{
    const db = localStorage.getItem('baseDeDatos');
    const {codigo_caja,codigo_filial} = conpcaja;
    const url:string = `${this.url}/${codigo_filial}/${codigo_caja}${this.paramtDb}${db}`;
    return this.http.delete<any>(url);
  }

  public getConPCajasPk(codigo_caja:string,codigo_filial:string):Observable<ConPCaja>{
    const db = localStorage.getItem('baseDeDatos');
    const url:string = `${this.url}/${codigo_caja}/${codigo_filial}${this.paramtDb}${db}`;
    return this.http.get<ConPCaja>(url);
  }

  public updateConPCaja(conpcaja:ConPCaja):Observable<ConPCaja>{
    const db = localStorage.getItem('baseDeDatos');
    const url:string = `${this.url}${this.paramtDb}${db}`;
    return this.http.put<ConPCaja>(url,{...conpcaja});
  }
}
