export interface ConPCaja {
    codigo_caja:   string;
    codigo_filial: string;
    nombre_caja:   string;
    codigo_puc:    string;
}

export interface Conexion {
    driverclassname: string;
    url:             string;
    username:        string;
    password:        string;
    key:             string;
    name:            string;
    schema?:         string;
}
