import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { ParametersRoutingModule } from './parameters-routing.module';
import { NewComponent } from './pages/new/new.component';
import { ListComponent } from './pages/list/list.component';
import { ScreensComponent } from './screens/screens.component';
import { SidemenuComponent } from './shared/sidemenu/sidemenu.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MessageModule } from '../message/message.module';



@NgModule({
  declarations: [
    NewComponent,
    ListComponent,
    ScreensComponent,
    SidemenuComponent
  ],
  imports: [
    CommonModule,
    ParametersRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    MatDialogModule,
    MessageModule
  ]
})
export class ParametersModule { }
