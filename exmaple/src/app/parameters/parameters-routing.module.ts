import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from './pages/list/list.component';
import { ScreensComponent } from './screens/screens.component';
import { NewComponent } from './pages/new/new.component';

const routes: Routes = [{
  path:'',
  component:ScreensComponent,
  children: [
    {path:'new',component:NewComponent},
    {path:'list',component:ListComponent},
    {path:'edit/:codigo_caja/:codigo_filial',component:NewComponent},
    {path:'**', redirectTo: 'list'}
]},{
  path:'**',
  redirectTo:''
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParametersRoutingModule { }
