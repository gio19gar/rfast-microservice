import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styles: [
  ]
})
export class QuestionComponent implements OnInit {

  public accion:string = 'crear';

  constructor(
    private dialogRef:MatDialogRef<QuestionComponent>,
    @Inject(MAT_DIALOG_DATA) public data:any
  ) { }

  ngOnInit(): void {
    this.accion = this.data['accion'];
  }

  public affirm():void{
    this.dialogRef.close(true);
  }

  public deny():void{
    this.dialogRef.close();
  }

}
