import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoComponent } from './info/info.component';
import { QuestionComponent } from './question/question.component';



@NgModule({
  declarations: [
    InfoComponent,
    QuestionComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    QuestionComponent
  ]
})
export class MessageModule { }
