import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Conexion } from '../parameters/interfaces/interfaces';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConexionesService {

  private url:string = environment.urlconexion;

  constructor(private http:HttpClient) { }

  getAllConexiones():Observable<Conexion[]> {
    return this.http.get<Conexion[]>(this.url);
  }
}
