/**
 * 
 */
package com.rfast.dto;

import lombok.Data;

/**
 * @author Rfast
 *
 */
@Data
public class ConexionesDTO {
	
	private String driverclassname;
	
	private String url;
	
	private String username;
	
	private String password;
	
	private String schema;
	
	private String key;
	
	private String name;
	
}
