package com.rfast.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.rfast.dto.ConexionesDTO;

@Service
public class ConexionesServiceImpl implements ConexionesService{

	@Override
	public List<ConexionesDTO> getConexiones() {
		List<ConexionesDTO> items = new ArrayList<ConexionesDTO>();
		ConexionesDTO item = new ConexionesDTO();
		item.setDriverclassname("org.postgresql.Driver");
		item.setUrl("jdbc:postgresql://localhost:5432/demo");
		item.setUsername("postgres");
		item.setPassword("donbulldog");
		item.setKey("tenantId1");
		item.setName("demo");
		
		ConexionesDTO item2 = new ConexionesDTO();
		item2.setDriverclassname("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		item2.setUrl("jdbc:sqlserver://172.16.1.38:1433;databaseName=Valle_cali_Ensalud0;integratedSecurity=false;trustServerCertificate=true");
		item2.setUsername("Soporte");
		item2.setPassword("soporte");
		item2.setSchema("Valle_cali_Ensalud0");
		item2.setKey("tenantId2");
		item2.setName("Valle_cali_Ensalud0");
		
		ConexionesDTO item3 = new ConexionesDTO();
		item3.setDriverclassname("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		item3.setUrl("jdbc:sqlserver://172.16.1.38:1433;databaseName=Cesar_La_Loma_Hospital;integratedSecurity=false;trustServerCertificate=true");
		item3.setUsername("Soporte");
		item3.setPassword("soporte");
		item3.setSchema("Cesar_La_Loma_Hospital");
		item3.setKey("tenantId3");
		item3.setName("Cesar_La_Loma_Hospital");
		
		items.add(item);
		items.add(item2);
		items.add(item3);
		
		return items;
	}

}
