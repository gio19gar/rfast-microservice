package com.rfast.services;

import java.util.List;

import com.rfast.dto.ConexionesDTO;

public interface ConexionesService {

	List<ConexionesDTO> getConexiones();

}
