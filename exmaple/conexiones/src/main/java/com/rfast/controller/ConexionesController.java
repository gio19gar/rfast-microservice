package com.rfast.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rfast.dto.ConexionesDTO;
import com.rfast.services.ConexionesService;


@RestController
@CrossOrigin(origins= "*")
@RequestMapping("/conexiones")
public class ConexionesController {
	
	@Autowired
	ConexionesService conexionesService;
		
	@GetMapping
	 public ResponseEntity<List<ConexionesDTO>> getConexiones() {
		List<ConexionesDTO> items = conexionesService.getConexiones();
		return new ResponseEntity<List<ConexionesDTO>>(items, HttpStatus.OK);
	 }

}
