package com.rfast;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ConexionesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConexionesApplication.class, args);
	}

}
