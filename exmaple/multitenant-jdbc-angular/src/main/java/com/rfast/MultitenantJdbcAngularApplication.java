package com.rfast;

import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.rfast.TenantDataSources.DataSourceMap;
import com.rfast.TenantInterceptorRoutingDataSource.CustomRoutingDataSource;

@EnableTransactionManagement
@ComponentScan(basePackages = "com.example")
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class MultitenantJdbcAngularApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultitenantJdbcAngularApplication.class, args);
	}
	
	@Bean
    public DataSource dataSource(){
        CustomRoutingDataSource customDataSource=new CustomRoutingDataSource();
        customDataSource.setTargetDataSources(DataSourceMap.getDataSourceHashMap());
        return customDataSource;
    }


}
