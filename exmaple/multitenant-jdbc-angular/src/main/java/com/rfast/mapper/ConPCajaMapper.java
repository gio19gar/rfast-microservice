package com.rfast.mapper;

import com.rfast.dto.ConPCajaDTO;
import com.rfast.jdbc.model.Con_p_caja;

public class ConPCajaMapper {
	
	public static Con_p_caja customerDTOToCon_p_caja(ConPCajaDTO item) {
		Con_p_caja result = new Con_p_caja(
				item.getCodigo_caja(),
				item.getCodigo_filial() ,
				item.getNombre_caja(),
				item.getCodigo_puc());
		return result;
	}

	public static ConPCajaDTO Con_p_cajaToDTO(Con_p_caja item) {
		ConPCajaDTO result = new ConPCajaDTO();
		result.setCodigo_caja(item.getCodigo_caja());
		result.setCodigo_filial(item.getCodigo_filial());
		result.setCodigo_puc(item.getCodigo_puc());
		result.setNombre_caja(item.getNombre_caja());
		return result;
	}

}
