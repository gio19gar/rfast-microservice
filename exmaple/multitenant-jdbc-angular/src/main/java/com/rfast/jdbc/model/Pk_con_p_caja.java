package com.rfast.jdbc.model;

import lombok.Data;

@Data
public class Pk_con_p_caja {
	
	private String codigo_caja;
	
	private String codigo_filial;

	public Pk_con_p_caja( String codigo_filial,String codigo_caja) {
		this.codigo_caja = codigo_caja;
		this.codigo_filial = codigo_filial;
	}
}
