package com.rfast.jdbc.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.rfast.jdbc.model.Con_p_caja;
import com.rfast.jdbc.model.Pk_con_p_caja;

@Repository
public class ConPCajaDao implements ConPCajaRepository {
	
    @Autowired
    JdbcTemplate jdbcTemplate;
    
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;


	@Override
	public List<Con_p_caja> getAllConPCaja() {
		List<Con_p_caja> allUsers = new ArrayList<>();

        String queryTotal = "SELECT codigo_caja,codigo_filial,nombre_caja,codigo_puc FROM con_p_caja ";
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(queryTotal);
        if(rows != null) {
            for (Map row : rows) {
                allUsers.add(
                		new Con_p_caja(
                				row.get("codigo_caja").toString(),
                				row.get("codigo_filial").toString(),
                				row.get("nombre_caja").toString(),
                				row.get("codigo_puc").toString())
        		);
            }
        }
        return allUsers;
	}


	@Override
	public Con_p_caja getConPCajaById(Pk_con_p_caja id) {
		String query = "SELECT codigo_caja,codigo_filial,nombre_caja,codigo_puc FROM con_p_caja where codigo_caja = :codigo_caja and codigo_filial = :codigo_filial";
		Map<String,Object> map= mapConPCajaById(id);
		Con_p_caja result = namedParameterJdbcTemplate.query(query,map,new ResultSetExtractor<Con_p_caja>() {
			@Override
			public Con_p_caja extractData(ResultSet rs) throws SQLException, DataAccessException {
				if(rs.next()) {
					Con_p_caja con_p_caja = new Con_p_caja(
							rs.getString("codigo_caja"), 
							rs.getString("codigo_filial"), 
							rs.getString("nombre_caja"), 
							rs.getString("codigo_puc")
					);
					return con_p_caja;
				}
				return null;
			}
		});
		return result;
	}
	
	public Map<String,Object> mapConPCajaById(Pk_con_p_caja id){
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("codigo_caja", id.getCodigo_caja());
		map.put("codigo_filial", id.getCodigo_filial());
		return map;
	}


	@Override
	public void save(Con_p_caja con_p_caja) {
		StringBuilder query = new StringBuilder();
		query.append("INSERT INTO con_p_caja")
			.append("(codigo_caja,codigo_filial,nombre_caja,codigo_puc)")
			.append(" VALUES ")
			.append(" (:codigo_caja,:codigo_filial,:nombre_caja,:codigo_puc)");
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("codigo_caja", con_p_caja.getCodigo_caja())
		.addValue("codigo_filial", con_p_caja.getCodigo_filial())
		.addValue("nombre_caja", con_p_caja.getNombre_caja())
		.addValue("codigo_puc", con_p_caja.getCodigo_puc());
		
		namedParameterJdbcTemplate.update(query.toString(), params);
	}
	
	@Override
	public void delete( Pk_con_p_caja id  ) {
		StringBuilder query = new StringBuilder();
		query.append("DELETE FROM con_p_caja") 
			.append(" WHERE ")
			.append(" codigo_caja = :codigo_caja and codigo_filial = :codigo_filial ");
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("codigo_caja", id.getCodigo_caja())
		.addValue("codigo_filial", id.getCodigo_filial());
		
		namedParameterJdbcTemplate.update(query.toString(), params);
	}


	@Override
	public void update(Con_p_caja con_p_caja) {
		StringBuilder query = new StringBuilder();
		query.append(" UPDATE con_p_caja ")
			.append(" SET nombre_caja=:nombre_caja,codigo_puc=:codigo_puc ")
			.append(" WHERE ")
			.append(" codigo_caja=:codigo_caja AND codigo_filial=:codigo_filial ");
			
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("codigo_caja", con_p_caja.getCodigo_caja())
		.addValue("codigo_filial", con_p_caja.getCodigo_filial())
		.addValue("nombre_caja", con_p_caja.getNombre_caja())
		.addValue("codigo_puc", con_p_caja.getCodigo_puc());
		
		namedParameterJdbcTemplate.update(query.toString(), params);
	}

}
