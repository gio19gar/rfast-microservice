package com.rfast.jdbc.dao;

import java.util.List;

import com.rfast.jdbc.model.Con_p_caja;
import com.rfast.jdbc.model.Pk_con_p_caja;

public interface ConPCajaRepository {
	
	List<Con_p_caja> getAllConPCaja();

	Con_p_caja getConPCajaById(Pk_con_p_caja id);

	void save(Con_p_caja con_p_caja);

	void delete(Pk_con_p_caja id);

	void update(Con_p_caja con_p_caja);

}
