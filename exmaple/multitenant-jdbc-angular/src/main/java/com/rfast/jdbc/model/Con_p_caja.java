package com.rfast.jdbc.model;

import lombok.Data;

@Data
public class Con_p_caja extends Pk_con_p_caja{
	
	private String nombre_caja;
	
	private String codigo_puc;

	public Con_p_caja(String codigo_caja, String codigo_filial, String nombre_caja, String codigo_puc) {
		super(codigo_caja, codigo_filial);
		this.nombre_caja = nombre_caja;
		this.codigo_puc = codigo_puc;
	}
	
}
