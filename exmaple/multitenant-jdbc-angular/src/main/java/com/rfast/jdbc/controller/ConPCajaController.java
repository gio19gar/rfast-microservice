package com.rfast.jdbc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rfast.dto.ConPCajaDTO;
import com.rfast.dto.service.ConPCajaService;
import com.rfast.jdbc.model.Con_p_caja;
import com.rfast.jdbc.model.Pk_con_p_caja;
import com.rfast.mapper.ConPCajaMapper;

@RestController
@CrossOrigin(origins= "*")
@RequestMapping("/api/v1/conpcajas")
public class ConPCajaController {
	
	@Autowired
	private ConPCajaService cajaService;
	
	 @GetMapping
	 public ResponseEntity<List<Con_p_caja>> getConPCajas() {
		List<Con_p_caja> items = cajaService.getAllConPCaja();
		return new ResponseEntity<List<Con_p_caja>>(items, HttpStatus.OK);
	 }
	 
	 @GetMapping("/{codigo_filial}/{codigo_caja}")
	 public ResponseEntity<Con_p_caja> getConPCajasPk(
			 @PathVariable("codigo_filial") String codigo_filial,
			 @PathVariable("codigo_caja") String codigo_caja
	 ) {
		 Pk_con_p_caja id = new Pk_con_p_caja(codigo_caja,codigo_filial);
		 Con_p_caja item = cajaService.getConPCajaById(id);
		 return new ResponseEntity<Con_p_caja>(item, HttpStatus.OK);
	 }
	 
	 @PostMapping
	 public ResponseEntity<ConPCajaDTO> saveConPCaja(@RequestBody ConPCajaDTO item){
		 Con_p_caja con_p_caja=ConPCajaMapper.customerDTOToCon_p_caja(item);
		 con_p_caja=cajaService.save(con_p_caja);
		 item = ConPCajaMapper.Con_p_cajaToDTO(con_p_caja);
		 return new ResponseEntity<ConPCajaDTO>(item, HttpStatus.OK);
	 }
	 
	 @DeleteMapping("/{codigo_filial}/{codigo_caja}")
	public ResponseEntity<String> deleteById(
			 @PathVariable("codigo_filial") String codigo_filial,
			 @PathVariable("codigo_caja") String codigo_caja
	) throws Exception{
	 Pk_con_p_caja id = new Pk_con_p_caja(codigo_filial,codigo_caja);
	 cajaService.delete(id);
	 return new ResponseEntity<String>("", HttpStatus.OK);
	}
	 
	 @PutMapping
	 public ResponseEntity<ConPCajaDTO> updateConPCaja(@RequestBody ConPCajaDTO item){
		 Con_p_caja con_p_caja=ConPCajaMapper.customerDTOToCon_p_caja(item);
		 con_p_caja=cajaService.update(con_p_caja);
		 item = ConPCajaMapper.Con_p_cajaToDTO(con_p_caja);
		 return new ResponseEntity<ConPCajaDTO>(item, HttpStatus.OK);
	 }

}
