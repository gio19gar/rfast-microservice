package com.rfast.TenantDataSources;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.rfast.dto.ConexionesDTO;

public class DataSourceMap {
	
	public static Map<Object, Object> getDataSourceHashMap() {
		Gson gson = new Gson();
		ResponseEntity<String> conexiones = sendDataGet();
		ConexionesDTO[] conexionesList = gson.fromJson(conexiones.getBody(),ConexionesDTO[].class);
		HashMap<Object, Object> hashMap = new HashMap<Object, Object>();
		for (ConexionesDTO conexionesDTO : conexionesList) {
			DriverManagerDataSource dataSource = new DriverManagerDataSource();
			if(conexionesDTO.getSchema() != null && !conexionesDTO.getSchema().trim().isEmpty()) {
				dataSource.setSchema(conexionesDTO.getSchema());
			}
			dataSource.setDriverClassName(conexionesDTO.getDriverclassname());
	        dataSource.setUrl(conexionesDTO.getUrl());
	        dataSource.setUsername(conexionesDTO.getUsername());
	        dataSource.setPassword(conexionesDTO.getPassword());
	        hashMap.put(conexionesDTO.getKey(), dataSource);
		}
        return hashMap;

	}
	
	static ResponseEntity<String> sendDataGet() {
		String url  = "http://localhost:8081/api/v1/conexiones";
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
		List<MediaType> headerAccept = new ArrayList<MediaType>();
		headerAccept.add(new MediaType("application", "json"));
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
		requestHeaders.setContentType(new MediaType("application", "json"));
		requestHeaders.setAccept(headerAccept);
		requestHeaders.setCacheControl("no-cache");
		ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null,
				String.class);
		return responseEntity;
	}
}
