package com.rfast.dto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rfast.jdbc.dao.ConPCajaRepository;
import com.rfast.jdbc.model.Con_p_caja;
import com.rfast.jdbc.model.Pk_con_p_caja;

@Service
public class ConPCajaServiceImpl implements ConPCajaService{
	
	@Autowired
	private ConPCajaRepository conPCajaDao;

	@Override
	public List<Con_p_caja> getAllConPCaja() {
		return conPCajaDao.getAllConPCaja();
	}

	@Override
	public Con_p_caja getConPCajaById(Pk_con_p_caja id) {
		return conPCajaDao.getConPCajaById(id);
	}

	@Override
	public Con_p_caja save(Con_p_caja con_p_caja) {
		conPCajaDao.save(con_p_caja);
		return con_p_caja;
	}

	@Override
	public void delete(Pk_con_p_caja id) {
		conPCajaDao.delete(id);
	}

	@Override
	public Con_p_caja update(Con_p_caja con_p_caja) {
		conPCajaDao.update(con_p_caja);
		return con_p_caja;
	}
	
	

}
