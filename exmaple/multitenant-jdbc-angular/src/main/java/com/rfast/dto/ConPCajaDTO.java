package com.rfast.dto;

import lombok.Data;

@Data
public class ConPCajaDTO {
	
	private String nombre_caja;
	
	private String codigo_puc;
	
	private String codigo_caja;
	
	private String codigo_filial;
}
