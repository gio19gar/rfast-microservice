package com.rfast.dto.service;

import java.util.List;

import com.rfast.jdbc.model.Con_p_caja;
import com.rfast.jdbc.model.Pk_con_p_caja;

public interface ConPCajaService {
	
	List<Con_p_caja> getAllConPCaja();

	Con_p_caja getConPCajaById(Pk_con_p_caja id);

	Con_p_caja save(Con_p_caja con_p_caja);
	
	void delete(Pk_con_p_caja id);

	Con_p_caja update(Con_p_caja con_p_caja);
}
