package com.rfast.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.rfast.model.GenpProfileMenu;

public class ProfileMenuMapper implements RowMapper<GenpProfileMenu> {

	@Override
	public GenpProfileMenu mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		GenpProfileMenu genpProfileMenu = new GenpProfileMenu();
		
		genpProfileMenu.setRecordId(rs.getInt("ID_REGISTRO"));
		genpProfileMenu.setItemMenu(rs.getString("ITEM_MENU"));
		genpProfileMenu.setProfileId(rs.getString("ID_PERFIL"));
		genpProfileMenu.setInsertion(rs.getBoolean("INSERCION"));
		genpProfileMenu.setErased(rs.getBoolean("BORRADO"));
		genpProfileMenu.setModification(rs.getBoolean("MODIFICACION"));
		genpProfileMenu.setQuery(rs.getBoolean("CONSULTA"));
		
		return genpProfileMenu;
		
	}

}
