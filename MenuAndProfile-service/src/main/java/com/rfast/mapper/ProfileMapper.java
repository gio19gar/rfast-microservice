package com.rfast.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.rfast.model.GenpProfile;

public class ProfileMapper implements RowMapper<GenpProfile> {

	@Override
	public GenpProfile mapRow(ResultSet rs, int rowNum) throws SQLException {
		GenpProfile genpProfile = new GenpProfile();
		
		genpProfile.setIdProfile(rs.getInt("ID_PERFIL"));
		genpProfile.setDesProfile(rs.getString("DES_PERFIL"));
		genpProfile.setDaysProfile(rs.getInt("DIAS_PERFIL"));
		
		
		return genpProfile;
	}

}
