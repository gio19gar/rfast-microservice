package com.rfast.feignclients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient (name = "profile-service", url = "http://localhost:8002")
@RequestMapping("/profiles")
public interface ProfileAndUserFeignClient {

}
