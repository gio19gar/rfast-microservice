package com.rfast.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rfast.dao.ProfileJdbcDAO;
import com.rfast.model.GenpProfile;

@Service
public class ProfileServiceImpl implements ProfileService{
	
	@Autowired
	ProfileJdbcDAO profileJdbcDAO;
	
	@Override
	public List<GenpProfile> list() {
		return profileJdbcDAO.list();
	}

	@Override
	public void create(GenpProfile profile) {
		profileJdbcDAO.create(profile);
		
	}

	@Override
	public Optional<GenpProfile> get(int id) {
	
		return profileJdbcDAO.get(id);
	}

	@Override
	public Optional<GenpProfile> update(GenpProfile profile) {
		
		return profileJdbcDAO.update(profile);
	}

	@Override
	public boolean delete(int id) {
		
		return profileJdbcDAO.get(id)
					.map(profile -> {
						profileJdbcDAO.delete(id);
						return true;
					}).orElse(false);
		
	}

}
