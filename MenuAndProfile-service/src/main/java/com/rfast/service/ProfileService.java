package com.rfast.service;

import java.util.List;
import java.util.Optional;

import com.rfast.model.GenpProfile;

public interface ProfileService {
	
	List<GenpProfile> list();
	
	void create(GenpProfile profile);
	
	Optional<GenpProfile> get(int id);
	
	Optional<GenpProfile> update(GenpProfile profile);
	
	boolean delete(int id);
}
