package com.rfast.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rfast.model.GenpProfile;
import com.rfast.service.ProfileService;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@RestController
@CrossOrigin("*")
@RequestMapping("/profiles")
public class ProfileController {
	
	@Autowired
	ProfileService profileService;
	
	
	@GetMapping
	public ResponseEntity<List<GenpProfile>> getAllProfiles() {
		
		return new ResponseEntity<List<GenpProfile>>(profileService.list(), HttpStatus.OK);
	} 
	
	@CircuitBreaker(name = "profilesCB", fallbackMethod = "fallbackGetProfile")
	@GetMapping("/{id}")
	public ResponseEntity<GenpProfile> getProfile(@PathVariable("id") int id){
		return profileService.get(id)
				.map(profile -> new ResponseEntity<>(profile, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}
	
	@PostMapping
	public ResponseEntity<String> createProfile(@RequestBody GenpProfile profile) {
		
		profileService.create(profile);
		return new ResponseEntity<String>("Perfil creado", HttpStatus.OK);
	} 
	
	@PutMapping
	public ResponseEntity<GenpProfile> updateProfile(@RequestBody GenpProfile genpProfile) {
		return profileService.update(genpProfile).
				map(profile -> new ResponseEntity<>(profile, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_MODIFIED));
	}
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<HttpStatus> deleteProfileById(@PathVariable("id") int id) {
		
		return profileService.delete(id) ? new ResponseEntity<>(HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND) ;
	}
	
	public ResponseEntity<GenpProfile> fallbackGetProfile(@PathVariable("id") int id, RuntimeException e){
		return new ResponseEntity("No hay perfiles", HttpStatus.OK);
				
	}
	
}
