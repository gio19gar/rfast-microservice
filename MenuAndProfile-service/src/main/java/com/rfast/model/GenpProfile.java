package com.rfast.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenpProfile {
	
	Integer idProfile;
	String desProfile;
	Integer daysProfile; 
	
	
}
