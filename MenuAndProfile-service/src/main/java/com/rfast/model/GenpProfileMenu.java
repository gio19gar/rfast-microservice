package com.rfast.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GenpProfileMenu {
	
	private Integer recordId;
	private String profileId;
	private String itemMenu;
	private Boolean  insertion;
	private Boolean erased;
	private Boolean modification;
	private Boolean query;
	
	
}
