package com.rfast.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.rfast.mapper.ProfileMapper;
import com.rfast.mapper.ProfileMenuMapper;
import com.rfast.model.GenpProfile;
import com.rfast.model.GenpProfileMenu;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ProfileMenuJdbcDAO implements DAO<GenpProfileMenu>{
	
	
	@Autowired	
	JdbcTemplate jdbcTemplate;
	
	@Override
	public List<GenpProfileMenu> list() {
		String query = "SELECT  ID_REGISTRO,ID_PERFIL,ITEM_MENU,INSERCION,BORRADO,MODIFICACION,CONSULTA FROM gen_p_perfil_menu;";
		
		return jdbcTemplate.query(query, new ProfileMenuMapper());
	}

	@Override
	public void create(GenpProfileMenu profileMenu) {
		String query = "INSERT INTO gen_p_perfil_menu (ID_PERFIL,ITEM_MENU,INSERCION,BORRADO,MODIFICACION,CONSULTA) VALUES (?,?,?,?,?,?) ;";
		
		int insert = jdbcTemplate.update(query);
		if ( insert == 1) {
			log.info("Perfil Menu creado");
		}
	}

	@Override
	public Optional<GenpProfileMenu> get(int id) {
		String query = "SELECT  ID_REGISTRO,ID_PERFIL,ITEM_MENU,INSERCION,BORRADO,MODIFICACION,CONSULTA FROM gen_p_perfil_menu where ID_REGISTRO = ?";
		GenpProfileMenu genpProfileMenu = null;
		try {
			List<GenpProfileMenu> list = jdbcTemplate.query(query, new ProfileMenuMapper(), new Object[]{id});
			if ( list.size() > 0 ) {
				genpProfileMenu = list.get(0);
			}
		} catch (DataAccessException e) {
			log.info("Perfil no encontrado");
		}
		return Optional.ofNullable(genpProfileMenu);

	}

	@Override
	public Optional<GenpProfileMenu> update(GenpProfileMenu t) {
		String query = "UPDATE gen_p_perfil_menu set ID_REGISTRO = ?,ID_PERFIL = ?,ITEM_MENU = ?,INSERCION = ?,BORRADO = ?,MODIFICACION = ?,CONSULTA = ?   where ID_REGISTRO = ?";
		return null;
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

}
