package com.rfast.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.rfast.mapper.ProfileMapper;
import com.rfast.model.GenpProfile;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class ProfileJdbcDAO implements DAO<GenpProfile> {
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public List<GenpProfile> list() {
		String query = "SELECT ID_PERFIL, DES_PERFIL, DIAS_PERFIL FROM gen_p_perfil";
		return jdbcTemplate.query(query, new ProfileMapper());
	}

	@Override
	public void create(GenpProfile profile) {
		String query = "INSERT INTO gen_p_perfil (ID_PERFIL, DES_PERFIL, DIAS_PERFIL) VALUES (?,?,?)";
		int insert = jdbcTemplate.update(query, profile.getIdProfile(), profile.getDesProfile(), profile.getDaysProfile());
		
		if ( insert == 1 ) {
			log.info("Nuevo perfile ha sido creado" + profile.getDesProfile());
		}
		
	}

	@Override
	public Optional<GenpProfile> get(int id) {
		String query = "SELECT ID_PERFIL, DES_PERFIL, DIAS_PERFIL FROM 	gen_p_perfil WHERE ID_PERFIL = ?";
		GenpProfile profile = null;
		
		try {
			List<GenpProfile> list = jdbcTemplate.query(query, new ProfileMapper(), new Object[]{id});
			if ( list.size() > 0 ) {
				profile = list.get(0);
			}
		} catch (DataAccessException e) {
			log.info("Perfil no encontrado");
		}
		return Optional.ofNullable(profile);
	}

	@Override
	public Optional<GenpProfile> update(GenpProfile profile) {
		String query = "UPDATE gen_p_perfil SET DES_PERFIL = ?, DIAS_PERFIL = ? WHERE ID_PERFIL = ?";
		int update = jdbcTemplate.update(query, profile.getDesProfile(), profile.getDaysProfile(), profile.getIdProfile());
		
		return update == 1 ? this.get(profile.getIdProfile()) : Optional.empty();
		
		
	}

	@Override
	public void delete(int id) {
		
		jdbcTemplate.update("DELETE FROM gen_p_perfil WHERE ID_PERFIL = ? ", id);
		
	}

}
